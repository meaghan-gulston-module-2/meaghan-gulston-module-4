import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mod_4/main2.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 100, 126, 71),
          accentColor: Colors.tealAccent),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    ));

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //var main2;
    // ignore: prefer_const_constructors
    Timer(
        Duration(seconds: 4),
        () => print(
            "Go to main2 home page")); //Navigator.of(context).pushReplacement(
    //MaterialPageRoute(builder: (BuildContext context) => main2)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Color.fromARGB(255, 121, 83, 128)),
          ),
          Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.teal,
                      radius: 70.0,
                      child: Icon(
                        Icons.coffee,
                        color: Colors.white70,
                        size: 50.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Text(
                      "Coffee Bean for your thoughts",
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.only(top: 25.0),
                  ),
                  Text(
                    "A fresh brew of conversation",
                    style: TextStyle(
                        color: Colors.white70,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
